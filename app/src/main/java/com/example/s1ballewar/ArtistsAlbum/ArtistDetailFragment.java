package com.example.s1ballewar.ArtistsAlbum;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * A fragment representing a single Artist detail screen.
 * This fragment is either contained in a {@link ArtistListActivity}
 * in two-pane mode (on tablets) or a {@link ArtistDetailActivity}
 * on handsets.
 */
public class ArtistDetailFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_POSITION = "item_position";

    /**
     * The artist content this fragment is presenting.
     */
    private ArtistContent.ArtistItem mItem = null;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ArtistDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_POSITION)) {
            // Load the artist content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = ArtistContent.artistItemsList.get( getArguments().getInt(ARG_ITEM_POSITION) );

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getName());
            }
            final ImageView image = (ImageView) activity.findViewById(R.id.image);
            if (image!=null) {
//                Ion.with(image).placeholder(R.mipmap.ic_launcher).load(mItem.getPicture());


                Log.d(TAG, "...............image width:" + image.getMeasuredWidth() + " & height:" + image.getMeasuredHeight());
                Log.d(TAG, "...............showing:" + mItem.getPicture());

                Glide.with(this).load(mItem.getPicture())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.mipmap.ic_launcher)
                        .into(image);

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artist_detail, container, false);

        // Show the content
        if (mItem != null) {
//            ((TextView) rootView.findViewById(R.id.textViewArtistDetailsName)).setText(mItem.getName());
            //set genres
            ((TextView) rootView.findViewById(R.id.textViewArtistDetailsGenres)).setText(mItem.getGenres());
//            ((TextView) rootView.findViewById(R.id.textViewArtistDetailsDescription)).setText(mItem.getDescription());

            //set description
            WebView webView = ((WebView) rootView.findViewById(R.id.webView));
            webView.loadData(mItem.getDescription(), "text/html", null);
//            webView.setFocusable(false);
            webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url != null && url.startsWith("http://")) {
                        view.getContext().startActivity(
                                new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            //set albums listview
            ListView listView = ((ListView) rootView.findViewById(R.id.listViewAlbums));
            listView.setAdapter(new AlbumListAdaptor(getContext(), mItem.getId()));
            setListViewHeightBasedOnChildren(listView);

            //set title for albums listview
            TextView textView = ((TextView) rootView.findViewById(R.id.textViewArtistAlbums));
            int albumCount = 0;
            if (listView!=null &&
                    listView.getAdapter()!=null)
                albumCount = listView.getAdapter().getCount();
            String artistAlbums = (albumCount == 0 ? "No" : albumCount) + " albums by " + mItem.getName();
            textView.setText(artistAlbums);

//            rootView.requestFocus();
        }

        return rootView;
    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}

package com.example.s1ballewar.ArtistsAlbum;

import java.util.ArrayList;

/**
 * Created by s1.ballewar on 14/10/15.
 */
public class ArtistContent {

    // list of ArtistItem
    public static ArrayList<ArtistItem> artistItemsList = new ArrayList<>();

//    // list of AlbumItem
//    public static ArrayList<AlbumItem> albumItemsList = new ArrayList<>();

    // multimap
    public static MultiMap<String, AlbumItem> multimapArtistAlbum = new MultiMap<>();

    // Whether or not the activity is in two-pane mode, i.e. running on a tablet device.
    public static boolean mTwoPane = false;

    /**
     * Artist item.
     */
    public static class ArtistItem {
        private String id;
        private String name;
        private String picture;
        private String genres;
        private String description;

        public ArtistItem(String id, String name, String picture, String genres, String description) {
            this.id = id;
            this.name = name;
            this.picture = picture;
            this.genres = genres;
            this.description = description;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getPicture() {
            return picture;
        }

        public String getGenres() {
            return genres;
        }

        public String getDescription() {
            return description;
        }
    }

    /**
     * Album item.
     */
    public static class AlbumItem {
        private String id;
        private String artistId;
        private String title;
        private String type;
        private String picture;

        public AlbumItem(String id, String artistId, String title, String type, String picture) {
            this.id = id;
            this.artistId = artistId;
            this.title = title;
            this.type = type;
            this.picture = picture;
        }

        public String getArtistId() {
            return artistId;
        }

        public String getPicture() {
            return picture;
        }

        public String getTitle() {
            return title;
        }
    }
}

package com.example.s1ballewar.ArtistsAlbum;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by s1.ballewar on 14/10/15.
 */
public class AlbumListAdaptor extends BaseAdapter {
    private final Context context;
    private List<ArtistContent.AlbumItem> albumItemList = null;


    public AlbumListAdaptor(Context context, String artistID) {
//        super(context, R.layout.artist_list_item, values);
        Log.d("AlbumListAdaptor", ".......creating with artistID:" + artistID);

        this.context = context;
        albumItemList = ArtistContent.multimapArtistAlbum.get(artistID);
    }

    public int getCount() {
        int count = 0;
        if (albumItemList!=null)
            count = albumItemList.size();
        Log.d("AlbumListAdaptor", ".......getCount:" + count);
        return count;
    }

    public Object getItem(int position) {
        Object item = null;
        if (albumItemList!=null)
            item = albumItemList.get(position);
        return item;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d("AlbumListAdaptor", ".......getView @ " + position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.album_list_item, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.textViewAlbumTitle);
        textView.setText( albumItemList.get(position).getTitle() );

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageViewAlbumPicture);
//        Ion.with(imageView).placeholder(R.mipmap.ic_launcher).load(albumItemList.get(position).getPicture());
        Glide.with(context).load(albumItemList.get(position).getPicture())
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);

        return rowView;
    }
}
package com.example.s1ballewar.ArtistsAlbum;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A list fragment representing a list of Artists. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link ArtistDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class ArtistListFragment extends ListFragment {
    private final String TAG = getClass().getSimpleName();


    private ProgressDialog pDialog;

    // URL to get artists JSON
    private static String jsonURL = "https://api.myjson.com/bins/2surw";

    // JSON Node names
    private static final String TAG_ARTISTS = "artists";
    private static final String TAG_ID = "id";
    private static final String TAG_GENRES = "genres";
    private static final String TAG_PICTURE = "picture";
    private static final String TAG_NAME = "name";
    private static final String TAG_DESCRIPTION = "description";

    private static final String TAG_ALBUMS = "albums";
//    private static final String TAG_ID = "id";
    private static final String TAG_ARTISTID = "artistId";
    private static final String TAG_TITLE = "title";
    private static final String TAG_TYPE = "type";
//    private static final String TAG_PICTURE = "picture";

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sArtistCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         * @param position
         */
        void onItemSelected(int position);
    }

    /**
     * A Artist implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sArtistCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(int position) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ArtistListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ArtistContent.artistItemsList.size()==0) {//Get the data only once, to avoid duplicates
            // Calling async task to get Artists from json
            new GetArtists().execute();

            // Calling async task to get Albums from json
            new GetAlbums().execute();
        }

        // set list adaptor
        setListAdapter(new ArtistListAdaptor(getContext()));
        Log.d("ArtistListFragment", "-----------------------------------setListAdapter");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
        Log.d("ArtistListFragment", "-----------------------------------onViewCreated");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the Artist implementation.
        mCallbacks = sArtistCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(position);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetArtists extends AsyncTask<Void, Void, Boolean> {

        // artists JSONArray
        JSONArray artists = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            boolean result = false;

            // Creating service handler class instance
            httpHandler sh = new httpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(jsonURL, httpHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    artists = jsonObj.getJSONArray(TAG_ARTISTS);

                    // looping through All artists
                    for (int i = 0; i < artists.length(); i++) {
                        JSONObject c = artists.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String picture = c.getString(TAG_PICTURE);
                        String genres = c.getString(TAG_GENRES);
                        String description = c.getString(TAG_DESCRIPTION);

                        // adding artists to artists list
                        ArtistContent.ArtistItem item = new ArtistContent.ArtistItem(id, name, picture, genres, description);
                        ArtistContent.artistItemsList.add(item);



                        Log.d(TAG, "...............downloading:" + item.getPicture());
                        Glide.with(getContext()).load(item.getPicture()).downloadOnly(1000, 1000);

                    }
                    result = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("httpHandler", "Couldn't get any data from the url");
            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            if (result)//success
                setListAdapter( new ArtistListAdaptor(getContext()) );
            else {//failed to get the data
                // adding a dummy item to show no data!
                ArtistContent.ArtistItem item = new ArtistContent.ArtistItem(null, "No data found!!!", null, null, null);
                ArtistContent.artistItemsList.add(item);
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetAlbums extends AsyncTask<Void, Void, Void> {

        // Albums JSONArray
        JSONArray albums = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            httpHandler sh = new httpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(jsonURL, httpHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    albums = jsonObj.getJSONArray(TAG_ALBUMS);

                    // looping through All albums
                    for (int i = 0; i < albums.length(); i++) {
                        JSONObject c = albums.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String artistID = c.getString(TAG_ARTISTID);
                        String title = c.getString(TAG_TITLE);
                        String type = c.getString(TAG_TYPE);
                        String picture = c.getString(TAG_PICTURE);

                        // adding albums to albums list
                        ArtistContent.AlbumItem item = new ArtistContent.AlbumItem(id, artistID, title, type, picture);
//                        ArtistContent.albumItemsList.add(item);

                        // adding albums to multimap as well
                        ArtistContent.multimapArtistAlbum.put(item.getArtistId(), item);




//                        Log.d(TAG, "...............downloading:" + item.getPicture());
//                        Glide.with(getContext()).load(item.getPicture()).downloadOnly(500, 500);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("httpHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
//            setListAdapter( new ArtistListAdaptor(getContext()) );
        }
    }
}

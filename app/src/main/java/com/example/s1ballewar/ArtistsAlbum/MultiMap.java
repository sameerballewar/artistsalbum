package com.example.s1ballewar.ArtistsAlbum;

/**
 * Created by s1.ballewar on 15/10/15.
 */

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A {@link Map} that supports multiple values per key.
 */
public class MultiMap<K, V> {
    private final Map<K, List<V>> mInternalMap;
    public MultiMap() {
        mInternalMap = new HashMap<K, List<V>>();
    }

    /**
     * Gets the list of values associated with each key.
     */
    public List<V> get(K key) {
        return mInternalMap.get(key);
    }
    /**
     * Adds the value to the list associated with a key.
     */
    public V put(K key, V value) {
        List<V> valueList = mInternalMap.get(key);
        if (valueList == null) {
            valueList = new LinkedList<V>();
            mInternalMap.put(key, valueList);
        }
        valueList.add(value);
        return value;
    }
}
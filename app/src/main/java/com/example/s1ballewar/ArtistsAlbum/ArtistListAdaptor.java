package com.example.s1ballewar.ArtistsAlbum;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by s1.ballewar on 14/10/15.
 */
public class ArtistListAdaptor extends BaseAdapter {
    private final Context context;

    public ArtistListAdaptor(Context context) {
//        super(context, R.layout.artist_list_item, values);
        this.context = context;
    }

    public int getCount() {
        int count = 0;
        if (ArtistContent.artistItemsList!=null)
            count = ArtistContent.artistItemsList.size();
//        Log.d("ArtistListAdaptor", ".......getCount:" + count);
        return count;
    }

    public Object getItem(int position) {
        Object item = null;
        if (ArtistContent.artistItemsList!=null)
            item = ArtistContent.artistItemsList.get(position);
        return item;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d("ArtistListAdaptor", ".......getView @ " + position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.artist_list_item, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.textViewName);
        textView.setText( ArtistContent.artistItemsList.get(position).getName() );

        textView = (TextView) rowView.findViewById(R.id.textViewGenres);
        textView.setText( ArtistContent.artistItemsList.get(position).getGenres() );

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
//        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
//        imageView.setImageBitmap(bm);

        //Image loading 0
//        Ion.with(imageView).placeholder(R.mipmap.ic_launcher).load(ArtistContent.artistItemsList.get(position).getPicture());

//        Glide.with(context).

        Glide.with(context).load(ArtistContent.artistItemsList.get(position).getPicture())
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);




//        //Image loading 1
//        UrlImageViewHelper.setUrlDrawable(imageView, itemDetailsrrayList.get(position).picture, R.mipmap.ic_launcher);

        //Image loading 2
//        URL url = null;
//        try {
//            url = new URL(itemDetailsrrayList.get(position).picture);
//            if (url!=null) {
//                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                imageView.setImageBitmap(bmp);
//            }
//        }catch (IOException e){
//            ;
//        }

        //Image loading 3
//        new DownloadImageTask(imageView).execute(itemDetailsrrayList.get(position).picture);

        return rowView;
    }
}
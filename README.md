# README #

Android app which parses music related data from json URL and shows artists details with their albums.

### What is this repository for? ###

* Quick summary: 
This is an Android app, which gets the data from the json data URL.
App has two screens: artist list screen & artist detail screen.
Artist list screen will show a list of all artists with the following information of each artist: thumbnail of the artist picture, name and genres.
Artist detail screen will show a bigger picture of the selected artist, the name, description, genres and a list of albums (thumbnail & title).
When an artist is tapped on the artist list screen app will navigate to the artist detail screen.

### How do I get set up? ###

* Load this project into Android Studio, it should build & run without any additional configuration.

### App screenshots ###

#### Mobile: ####
![image.png](https://bitbucket.org/repo/rr6oy9/images/211783773-image.png)
![image2.png](https://bitbucket.org/repo/rr6oy9/images/1242608923-image2.png)

#### Tablet: ####
![imageTab.png](https://bitbucket.org/repo/rr6oy9/images/4165374254-imageTab.png)